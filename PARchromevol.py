#! /usr/bin/env python3

import os
import re
import shutil
from partools import parallel
from subprocess import PIPE, Popen


def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


def replace_all(rep, text):
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))
    text = pattern.sub(lambda m: rep[re.escape(m.group(0))], text)
    return text


def split_trees(org_file, prefix="tree", trees_per_file=1):
    """ Splits newick tree file into several tree files"""
    fin = open(org_file)
    counter = 0

    for line in fin:
        new_dir = "%s_%s" % (prefix, counter)
        new_file = "%s/tree_%s.tre" % (new_dir, counter)
        #new_file = "%s/%s_%s.tre" % (new_dir, prefix, counter)
        os.mkdir(new_dir)
        fout = open(new_file, "w")
        print(line, end="\n", file=fout)
        fout.close()
        counter += trees_per_file
    return counter


"""
Example of a template to be used for each tree in the analysis:
Change everything to suit your analysis except the text b/t <>

_mainType Optimize_Model
_outDir <OUT_DIR>
_dataFile <CNT_FILE>
_treeFile <TREE_FILE>
_logFile <LOG_FILE>
_maxChrNum 0
_minChrNum 0
_branchMul 1
_simulationsNum 10000
_logValue 6
_maxOptimizationIterations 5
_epsilonLLimprovement 0.01
_optimizePointsNum 10,3,1
_optimizeIterNum 0,2,5
_gainConstR 1
_lossConstR 1

"""


def prep_params(total_trees, template, counts_fn, model, prefix="tree"):
    """ Prepares param file for all trees  """
    fin = open(template)
    text = fin.read()
    params_list = []
    for x in range(total_trees):
        outdir = "%s_%s" % (prefix, x)
        rep = {"<OUT_DIR>": "%s/%s/" % (outdir, model),
               "<CNT_FILE>": "%s/%s" % (outdir, counts_fn),
               "<TREE_FILE>": "%s/tree_%s.tre" % (outdir, x),
               "<LOG_FILE>": "tree_%s.log" % (x)}
        new_text = replace_all(rep, text)
        fout = open("%s/param_%s.txt" % (outdir, model), "w")
        print(new_text, end="", file=fout)
        shutil.copyfile(counts_fn, "%s/%s" % (outdir, counts_fn))
        params_list.append(fout.name)
        fout.close()
    return params_list


def ChromEvol(param_file):
    cmdline("./chromEvol %s" % (param_file))
    return 0


def main():

    import argparse as ap
    import os
    from multiprocessing import cpu_count
    parser = ap.ArgumentParser()

    parser.add_argument('-t', "--trees", help="original newick tree file",
                        required=True, type=str)
    parser.add_argument('-c', "--counts", help='chromosome number counts file',
                        required=True, type=str)
    parser.add_argument('-p', "--params", help="template for params file",
                        required=True, type=str)
    parser.add_argument('-s', "--prefix", help="folder directory locations",
                        required=True, type=str)
    parser.add_argument('-l', "--location", help="location of chromevol exe",
                        required=True, type=str)
    parser.add_argument(
        '-m', "--model", help="model name for naming of output dir",
        required=True, type=str
    )
    parser.add_argument(
        '-n', "--ncpus", help="number of cpus to use, default=all",
        required=False, default=cpu_count(), type=int
    )
    args = parser.parse_args()

    try:
        os.mkdir(args.prefix)
    except FileExistsError:
        print("Folder already exists!")
        print("Not overwriting!")
        return 1

    new_prefix = args.prefix + "/tree"
    total = split_trees(args.trees, prefix=new_prefix)
    params_list = prep_params(total, args.params, args.counts, args.model, prefix=new_prefix)
    print(params_list)
    par_ce = parallel(ChromEvol, "ChromEvol", args.ncpus, params_list)

    return 0

if __name__ == '__main__':
    main()
