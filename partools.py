#! /usr/bin/env python3

"""

This is a collection of parallel functions originally
written by Petr Novak (see RepeatExplorer repository)

"""


import multiprocessing
import os
import time
import random

def spawn(func):
    """
    returns a func of the form f(x,y);
    takes two args, an end of a pipe and
    a set of arguments

    """

    def fun(pipe, x):
        """ sends function ready to be called across pipe """
        pipe.send(func(x))
        pipe.close()

    return fun


def rename(new_name):
    """ Function for renaming processes
        passed to parallel function """

    def decorator(f):
        f.__name__ = new_name
        return f

    return decorator


def mp_parmap(cmd, ncpus, arg_iter):

    max_proc = ncpus

    pipes = []
    procs = []
    retvals = {}

    counter = 0

    for args in arg_iter:
        counter += 1
        pipes.append(multiprocessing.Pipe())
        # When "recv" is called, needs to be called
        # from other end not spec'd here.
        procs.append(
            multiprocessing.Process(
                target=spawn(cmd),
                args=(pipes[-1][0], args),
                name=str(counter)
                )
        )

        # needs to be -1 because append is to end of list, 0 would not work
        curr_proc = procs[-1]

        # counts alive processes
        while True:
            running = 0

            for proc in procs:
                if proc.is_alive():
                    running += 1

            if running < max_proc:
                break
            else:
                time.sleep(0.1)

        curr_proc.start()
        print(
            "%s_%s_%s process has been started" % (
                cmd.__name__, curr_proc.pid, curr_proc.name
            )
        )

        # Join finished processes,
        # append results to retvals and close pipe ends!
        for proc, pipe in zip(procs, pipes):
            conditions = (
                proc.pid,
                not(proc.exitcode),
                not(proc.is_alive()),
                (proc.name not in retvals)
            )
            if not(any(conditions)):
                proc.join()
                retvals[int(proc.name)] = pipe[1].recv()
                pipe[0].close()
                pipe[1].close()
                pipes.remove(pipe)
                procs.remove(proc)

    # There will be a point when no more proc are added to procs
    # Collect the remaining proc; join, append, close!
    for proc in procs:
        proc.join()

    for proc, pipe in zip(procs, pipes):
        conditions = (
            proc.pid,
            not(proc.exitcode),
            not(proc.is_alive()),
            (proc.name not in retvals)
        )
        if not(any(conditions)):
            retvals[int(proc.name)] = pipe[1].recv()
            pipe[0].close()
            pipe[1].close()

    # make list of retvals in correct order
    retvals = [retvals[key] for key in sorted(retvals.keys())]
    return retvals


def parallel(cmd, cmd_name, ncpus, *args):

    len_lists = [len(li) for li in args]
    args_lists = [list(x) for x in args]
    high = max(len_lists)

    if len(set(len_lists)) == 1:
        # len of lists are all the same
        # print("args are correct length!")
        print("Args of correct length")

    elif set(len_lists) == set([1, high]):
        # expanding lists of len = 1

        for i in range(len(args_lists)):
            if len(args_lists[i]) == 1:
                args_lists[i] *= high

        args = [tuple(x) for x in args_lists]

    else:
        print("Args not of correct length!")
        return 1

    args_tups = zip(*args)

    # decorator changes name of function for better output
    # @rename('BeastPathSampling')
    @rename(cmd_name)
    def commander(args):
        return cmd(*args)

    return mp_parmap(commander, ncpus, args_tups)

# a sample function for testing
def iter_sample_fast(iterable, samplesize):
	results = []
	iterator = iter(iterable)
	# Fill in the first samplesize elements:
	# generates a list of items of desired sample size, shuffles,
	# then randomly replaces these items with other items from the iterable
	# Fill in the first samplesize elements:
	try:
		for x in range(samplesize):
			results.append(next(iterator))

	except StopIteration:
		raise ValueError("Sample larger than population.")

	random.shuffle(results)  # Randomize their positions

	for i, v in enumerate(iterator, samplesize):
		r = random.randint(0, i)
		if r < samplesize:
			results[r] = v  # at a decreasing rate, replace random items
	counter = 0
	for val in results:
		counter += val
	return counter


if __name__ == "__main__":
	parallel(
		iter_sample_fast, "Sample Iterable", 8, [10], [5,6,7,8,9], [10,20,30,40,50]
	)
