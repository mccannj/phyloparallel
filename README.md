PhyloParallel


USAGE:

For the path sampling script you have to generate the step directories
using Beast2, i.e.

if Beast2 puts seed in your log file names like so:
your_xml_file_$(seed).log

then you should generate a seed:
seed=$(python -c "import random; print(random.getrandbits(32))")

and use it when you call beast to generate the step directories:
beast -threads 8 -seed $seed your_xml_file.xml

./PATHsampling.py -p full_path_to_step_dirs -l your_xml_file -t 0 7 -s $seed -n 8

-l is the prefix to the logfile name

-t is the range of the steps you want to analyze,
which is useful if you have more steps than cores
and you want to analyze more steps across job
submissions

-n is the number of cpus to use