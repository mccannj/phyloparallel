#! /usr/bin/env python3

from subprocess import PIPE, Popen
import multiprocessing
import time
import re

import numpy
import math

from partools import parallel


def cmdline(command, stepnum, steppath):
    import os
    os.chdir(steppath)
    process = Popen(
        args=command,
        stdout=PIPE,
        stderr=PIPE,
        shell=True
    )
    op = process.communicate()[0].decode("utf-8")

    process.stdout.close()
    process.stderr.close()

    opfile = open("step%s_output.trace" % (stepnum), "w")

    print(op, file=opfile)

    opfile.close()

    return op


def generate_step_dir(threads, filename):
    command = "beast -threads %s %s" % (threads, filename)
    output = cmdline(command)
    return output


def check_output(output_text):
    return 0


def beast(
    xml, seed, resume=False, beast_path="beast",
    overwrite=True, beagle=True
):

    cmd = beast_path
    if resume:
        cmd += " -resume"
    if overwrite:
        cmd += " -overwrite"
    if beagle:
        cmd += " -beagle_SSE"
    cmd += " -seed %s %s" % (seed, xml)

    return cmd


def check_trace(tracefilename):
    f = open(tracefilename)
    # counter starts here to account for header line
    counter = -1
    for line in f:
        if line.startswith("#"):
            continue
        else:
            counter += 1
    f.close()
    return counter


def check_trees(tree_fn_list):
    from Parser import readFile
    num_trees = []
    for fn in tree_fn_list:
        inhand = open(fn)
        trees = readFile(inhand)
        inhand.close()
        num_trees.append(len(trees))
    if len(set(num_trees)) != 1:
        print("bad trees")
    else:
        print("good_trees")
        return num_trees[0]


def process_shell_scripts(steppath):
    import os
    run = steppath + "/run.sh"
    resume = steppath + "/resume.sh"
    fullpath = os.path.dirname(os.path.abspath(run))
    infile_run = open(run)
    infile_resume = open(resume)
    outfile_run = open(fullpath + "/run_edit.sh", "w")
    outfile_resume = open(fullpath + "/resume_edit.sh", "w")
    text_run = infile_run.read()
    text_resume = infile_resume.read()
    paths = text_run.split(r'"')[3].split(":")
    for val in paths:
        if "beast.jar" in val:
            grab = val
    replacing_run = "beast.app.beastapp.BeastMain -overwrite -java"
    replacing_resume = "beast.app.beastapp.BeastMain -resume -java"

    grab_run = "-jar " + grab + " -overwrite"
    grab_resume = "-jar " + grab + " -resume"

    text_run = text_run.replace(replacing_run, grab_run)
    text_resume = text_resume.replace(replacing_resume, grab_resume)

    print(text_run, file=outfile_run)
    print(text_resume, file=outfile_resume)
    infile_run.close()
    infile_resume.close()
    outfile_run.close()
    outfile_resume.close()
    return 0





def getESS(data, burninPerc, thin=None):
    """  Effective sample size, as computed by
         BEAST Tracer, stolen from Tim Vaughan who
         stole it from biopy :). """

    burnin = int(math.floor(burninPerc*len(data)/100.0))
    data = data[burnin:]

    if thin != None:
        data = data[0:len(data):max(len(data)/thin,1)]

    samples = len(data)

    assert len(data) > 1, "no stats for short sequences"

    maxLag = min(samples // 3, 1000)

    gammaStat = [0] * maxLag

    varStat = 0.0

    if type(data) != numpy.ndarray:
        data = numpy.array(data)

    normalizedData = data - data.mean()

    for lag in range(maxLag):
        v1 = normalizedData[:samples - lag]
        v2 = normalizedData[lag:]
        v = v1 * v2
        gammaStat[lag] = sum(v) / len(v)

        if lag == 0:
            varStat = gammaStat[0]
        elif lag % 2 == 0:
            s = gammaStat[lag - 1] + gammaStat[lag]
            if s > 0:
                varStat += 2.0 * s
            else:
                break

    if varStat > 0:
        return samples * gammaStat[0] / varStat
    else:
        return float("nan")


def check_all_ESS(tracefile, burnin):
    """ calculates mean ESS for all
        parameters in tracefile """
    a = numpy.genfromtxt(tracefile)
    # removes values that are not float from trace
    b = a[~numpy.isnan(a).any(axis=1)]
    # removes state row from trace
    c = numpy.delete(b, 0, 1)
    d = numpy.apply_along_axis(getESS, 0, c, burnin)
    return d.mean()


def get_log_data():
    return 0


def MLE(logdata_mat, alpha, nsteps, b=1.0):
    """ Converted from model selection Beast2 package
        Not quite finished with calculation """

    from scipy.stats import beta
    if alpha:
        contrib = []

        for i in range(0, nsteps):
            data = get_log_data()
            beta1 = beta.ppf(
                ((nstep - 1.0 - i) / (nsteps - 1)), alpha, b
            )
            beta2 = beta.ppf(
                ((nstep - 1.0 - (i + 1.0)) / (nsteps - 1)), alpha, b
            )
            weight = beta1 - beta2
    else:
        pass


def write_seed_to_file(steppath, seed):
    f = open("%s/.seedfile" % steppath, "w")
    print(seed, file=f)
    f.close()
    return 0


def load_seed_from_file(steppath):
    f = open("%s/.seedfile" % steppath)
    seed = int(f.read())
    f.close()
    return seed


def main():
    import argparse as ap
    import random
    import multiprocessing
    import glob, os

    parser = ap.ArgumentParser()

    parser.add_argument(
        '-p', "--path",
        help="stepdir path",
        required=True, type=str
    )

    parser.add_argument(
        '-l', "--log",
        help="log prefix",
        required=True, type=str
    )

    parser.add_argument(
        '-t', "--threads",
        help="threads range for steps", nargs="+",
        required=True, type=int
    )

    parser.add_argument(
        '-s', "--seed",
        help="seed for generating step dirs with beast",
        required=True, type=int
    )

    parser.add_argument(
        '-b', "--burn",
        help="burn-in percent for ESS calculation",
        required=False, type=int, default=50
    )

    parser.add_argument(
        '-n', "--ncpus",
        help="number of cpus to use, default: 4",
        required=False, type=int, default=4
    )

    parser.add_argument(
        '-m', "--maxruntime",
        help="max runtime in secs",
        required=False, type=int, default=8*60*60
    )

    parser.add_argument(
        '-r', "--resume",
        help="resume switch for pre-existing runs",
        action='store_true',
    )

    parser.add_argument(
        '-e', "--maxess",
        help="max ess of likelihood logs",
        required=False, type=float, default=200.0
    )

    # parser.add_argument(
    #     '-g', "--genvsc",
    #     help="generate vsc job script for resuming",
    #     action='store_true',
    # )

    # parser.add_argument(
    #     '-v', "--runvsc",
    #     help="run vsc job script for resuming",
    #     action='store_true',
    # )

    args = parser.parse_args()

    # collects steps to work on
    tr = [args.threads[0], args.threads[1]]

    cmds = []
    logs = []
    lh_logs = []
    seeds = []
    xmls = []
    steps = [x for x in range(tr[0], tr[1] + 1)]
    steppaths = [
        "%s/step%s" % (args.path, x) for x in range(
            tr[0], tr[1] + 1
        )
    ]

    run_cmds = ["sh %s/run_edit.sh" % (steppaths[x]) for x in range(0, len(steppaths))]
    resume_cmds = ["sh %s/resume_edit.sh" % (steppaths[x]) for x in range(0, len(steppaths))]

    for val in steppaths:
        process_shell_scripts(val)

    stepdict = {
        # x: 0.0 for x in range(tr[0], tr[1] + 1)
        x: 0.0 for x in range(0, tr[1]+1-tr[0])
    }

    for x in range(tr[0], tr[1] + 1):
        seed = random.getrandbits(32)
        seeds.append(seed)
        xml = "%s/step%s/beast.xml" % (args.path, x)
        xmls.append(xml)
        beast_call = beast(xml, seed)
        cmds.append(beast_call)
        beast_log = "%s/step%s/%s_%s.log" % (
            args.path, x, args.log, args.seed
        )
        logs.append(beast_log)
        lh_logs.append("%s/step%s/likelihood.log" % (args.path, x))

    start = time.time()

    # if resuming a previously finished run
    if args.resume:
        cmds = []
        seeds = []
        # need the range to start at 0
        # and end at the length of the range
        for x in range(0, (tr[1] - tr[0] + 1)):
            seed = load_seed_from_file(steppaths[x])
            seeds.append(seed)
            cmds.append(
                beast(
                    xmls[x], seed, resume=True, overwrite=False
                )
            )
    else:
        for x in range(0, len(seeds)):
            write_seed_to_file(steppaths[x], seeds[x])

    parallel(
        cmdline, "BEAST PATH SAMPLING", args.ncpus, run_cmds, steps, steppaths
    )

    end = time.time()
    elapsed = end - start
    timed_events = 1

    print("average time = %s" % elapsed)
    broken = False
    while True:
        print("####################")
        print("Starting checks round %s " % timed_events)
        remaining = []
        print("checking ESS values")
        for key in sorted(stepdict.keys()):
            old_ess = stepdict[key]
            stepdict[key] = check_all_ESS(lh_logs[key], args.burn)
            if stepdict[key] < args.maxess:
                print(
                    "Step %s --> ESS = %s :: Old ESS --> %s" % (
                        key + tr[0], stepdict[key], old_ess
                    )
                )
                remaining.append(key)
            else:
                print(
                    "Step %s --> ESS = %s --> DONE!" % (
                        key + tr[0], stepdict[key]
                    )
                )

        res_cmds = [resume_cmds[x] for x in remaining]
        res_steppaths = ["%s/step%s" % (args.path, x) for x in remaining]
        print(remaining)
        print(res_cmds)
        print(res_steppaths)
        print(
            "The following steps remain:\n" + "\n".join(
                ["step %s" % (x) for x in [y + tr[0] for y in remaining]]
            )
        )

        print(len(remaining))
        print(len(res_cmds))
        print(len(res_steppaths))

        for y in range(0, len(steppaths)):
            trace_lines = check_trace(logs[y])
            tree_lines = check_trees(glob.glob("%s/*.trees" % (steppaths[y])))
            lh_log_lines = check_trace(lh_logs[y])

            if trace_lines == tree_lines:
                print("Beast Run Intact")
                print("num gens = %s" % (trace_lines*(100000-1)))
                print("lh log has %s gens" % (lh_log_lines*(400-1)))
            else:
                broken=True
                print("Beast Run Broken")
                break

        if broken:
            break

        start = time.time()

        parallel(
            cmdline, "BEAST PATH SAMPLING",
            args.ncpus, res_cmds, remaining, res_steppaths
        )

        end = time.time()
        elapsed += end - start
        timed_events += 1
        avg_time = elapsed / timed_events
        print("average time = %s" % avg_time)

        if elapsed + avg_time + 120 > args.maxruntime:
            print("max time reached, this was the last run")
            print("current time")
            break

        if len(cmds) == 0:
            break

    return 0

if __name__ == '__main__':
    main()
